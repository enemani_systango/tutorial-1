
require 'csv'
CSV.foreach('Example.csv',  converters: :numeric) do |row|
puts row.inspect
end 

def sales_tax_India(price)

        if price < 100
		@sales_tax = 0

	elsif price>=100 && price<500
		@sales_tax = (price-100) * 0.05

	else 
		@sales_tax = 20+(price-500)*0.2

	end

	return @sales_tax

end



def sales_tax_US(price)

	@sales_tax = Math.sqrt(price)
	return @sales_tax

end



def sales_tax_UK(price)

	@sales_tax = (price)*0.03
	return @sales_tax

end


final_sales_tax = Array.new
final_sales_tax << "Sales_Tax"


CSV.foreach('Example.csv', converters: :numeric, headers:true) do |row|

	if row[2] == 'India'
		@sales_tax = sales_tax_India(row[1])

	elsif row[2] == 'US'
		@sales_tax = sales_tax_US(row[1])

	elsif row[2] == 'UK'
		@sales_tax = sales_tax_UK(row[1])

	else 

		#puts "Country Not in our list"
		@sales_tax = 'N.A'

	end
	

	final_sales_tax << @sales_tax

end 



output_array = CSV.read('Example.csv')

output_array.each do |output_column|

	output_column << final_sales_tax.shift

end


puts output_array.inspect



CSV.open('output.csv', 'w') do |csv_object|

  output_array.each do |row_array|

    csv_object << row_array

  end

end


